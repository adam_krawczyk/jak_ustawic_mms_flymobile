# Instrukcja jak ustawić MMS w sieci Flymobile na urządzeniu z IOS

## Krok 1

Wybierz `Ustawienia` / `Settings`

![img0](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img0.jpg)

## Krok 2 

Wybierz `Sieć Komórkowa` / `Mobile Data`

![img1](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img1.jpg)

## Krok 3

Włącz `Dane sieci komórkowej` / `Mobile Data`

![img2](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img2.jpg)

## Krok 4

Wprowadź dane w polu MMS 

```
APN: flymobile.mms
Username: -
Password: -
MMSC: http://mms.flymobile.pl:8002
MMS Proxy: 212.2.96.16:8080
MMS Max Message Size: 307200
MMS UA Prof URL: -
```

![img3](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img3.jpg)

## Krok 5 

Wybierz `Ustawienia` / `Settings` -> `Wiadomości` / `Messages`

![img4](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img4.jpg)

## Krok 6 

Włącz `Wiadomości MMS` / `MMS Messaging` 

![img5](https://gitlab.com/adam_krawczyk/jak_ustawic_mms_flymobile/-/raw/master/images/img5.jpg)

Jeżeli pole nie jest aktywne lub nie ma takiego pola, zrestartuj telefon. 

#

Wysyłanie MMS powinno działać.

# 

## W przypadku problemów skontaktuj się z nami

### Dane kontaktowe:

Inter Plus sp. z o.o.
ul. Świętokrzyska 30/63
00-116 Warszawa

NIP 525-259-13-97

Oddział Chełm:

ul. Popiełuszki 13, piętro 3
22-100 Chełm

Tel.: 791-262-868
email.: biuro@inter-plus.eu

#

Adam Krawczyk 